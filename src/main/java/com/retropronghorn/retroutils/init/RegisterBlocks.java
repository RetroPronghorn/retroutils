package com.retropronghorn.retroutils.init;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
// RetroUtils
import com.retropronghorn.retroutils.blocks.StallBlock;

@Mod.EventBusSubscriber(modid="retroutils")
public class RegisterBlocks {
    static Block stallBlock;
    static final CreativeTabs retroUtilsTab = (new CreativeTabs("retroUtilsTab") {
        @Override
        public ItemStack getTabIconItem() {
            return new ItemStack(stallBlock);
        }
    });

    public static void init() {
        stallBlock = new StallBlock("stall_block", Material.ROCK, retroUtilsTab);
    }

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        event.getRegistry().registerAll(stallBlock);
    }

    @SubscribeEvent
    public static void registerItemBlocks(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll(new ItemBlock(stallBlock).setRegistryName(stallBlock.getRegistryName()));
    }

    @SubscribeEvent
    public static void registerRenders(ModelRegistryEvent event) {
        registerRender(Item.getItemFromBlock(stallBlock));
    }

    private static void registerRender(Item item) {
        ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation( item.getRegistryName(), "inventory"));
    }
}
