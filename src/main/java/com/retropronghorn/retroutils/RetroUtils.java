package com.retropronghorn.retroutils;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

import com.retropronghorn.retroutils.init.RegisterBlocks;

@Mod(modid = RetroUtils.MODID, name = RetroUtils.NAME, version = RetroUtils.VERSION)
public class RetroUtils {
    public static final String MODID = "retroutils";
    public static final String NAME = "RetroUtils";
    public static final String VERSION = "1.0";

    private static Logger logger;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        // Register custom blocks
        RegisterBlocks.init();
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {

    }
}
