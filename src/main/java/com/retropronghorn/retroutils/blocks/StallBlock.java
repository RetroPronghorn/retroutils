package com.retropronghorn.retroutils.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.properties.PropertyBool;

import java.util.EnumSet;

public class StallBlock extends Block {
    private final int REDSTONE_TRIGGER = 8;
    private final int MAX_TICK_DELAY = 72000;

    public static final PropertyInteger POWER = PropertyInteger.create("power", 0, 15);
    public static final PropertyBool LOCKED = PropertyBool.create("locked");

    public StallBlock(String registryName, Material materialIn, CreativeTabs tab) {
        super(materialIn);

        setCreativeTab(tab);
        setUnlocalizedName(registryName);
        setRegistryName(registryName);

        // Setup default block state
        this.setDefaultState(
            this.blockState.getBaseState()
                .withProperty(LOCKED, Boolean.valueOf(false))
                .withProperty(POWER, 0)
        );
    }

    /**
     * Called when a neighboring block was changed and marks that this state should perform any checks during a neighbor
     * change. Cases may include when redstone power is updated, cactus blocks popping off due to a neighboring solid
     * block, etc.
     */
    @Override
    public void observedNeighborChange(IBlockState blockState, World worldIn, BlockPos pos, Block changedBlock, BlockPos changedBlockPos) {
        super.observedNeighborChange(blockState, worldIn, pos, changedBlock, changedBlockPos);
        int blockPowerLevel = worldIn.isBlockIndirectlyGettingPowered(pos);

        // If the block is already powered we don't wan't to start the process again
        if (blockPowerLevel >= REDSTONE_TRIGGER && !blockState.getValue(LOCKED)) {
            waitAndEmitSignal(blockPowerLevel, worldIn, pos);
        } else {
            System.out.println("BLOCK LOCKED");
        }
    }

    /**
     * Wait the configured amount of ticks then pulse a redstone signal
     */
    private void waitAndEmitSignal(int blockPowerLevel, World worldIn, BlockPos pos) {
        System.out.println("BLOCK POWERED! Level: " + String.valueOf(blockPowerLevel));
        notifyNeighbors(worldIn, pos);
    }

    protected void notifyNeighbors(World worldIn, BlockPos pos) {
        for (EnumFacing enumfacing : EnumFacing.values()) {
            BlockPos blockpos = pos.offset(enumfacing.getOpposite());
            if (ForgeEventFactory.onNeighborNotify(worldIn, pos, worldIn.getBlockState(pos),
                    EnumSet.of(enumfacing.getOpposite()), false).isCanceled())
                return;
            worldIn.neighborChanged(blockpos, this, pos);
            worldIn.notifyNeighborsOfStateExcept(blockpos, this, enumfacing);
        }
    }

    @Override
    public boolean canProvidePower(IBlockState blockState) {
        return blockState.getValue(POWER) == 15;
    }

    @Override
    public int getWeakPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side) {
        return blockState.getValue(POWER);
    }

    @Override
    public int getStrongPower(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side) {
        return getWeakPower(blockState, blockAccess, pos, side);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        /*
         * Valid States:
         * [0] - Power state (1 = 0ff, 2 = On)
         * [1] - Locked state (1 = Unlocked, 2 = Locked)
         */
        String stringState = "";
        stringState += state.getValue(POWER) == 15 ? 2 : 1;
        stringState += state.getValue(LOCKED) ? 2 : 1;
        return Integer.getInteger(stringState);
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, new IProperty[] { POWER, LOCKED });
    }
}
